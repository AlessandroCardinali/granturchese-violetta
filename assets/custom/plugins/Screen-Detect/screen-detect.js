// Live Detect Browser Size - jQuery
// http://stackoverflow.com/questions/12781205/live-detect-browser-size-jquery-javascript
// http://jsfiddle.net/Matt_Coughlin/Uvh6e/1/
function jqUpdateSize(){
	// Get the dimensions of the viewport
	var width = $(window).width();
	var height = $(window).height();
	
	// Aggiunge la classe small-screen al body se height è inferiore a xxx
	if (height < 640) {
		$(document.body).addClass('small-screen');
	} else {
		$(document.body).removeClass('small-screen');
	}
	
	//$('#jqWidth').html(width);	  // Display the width
	//$('#jqHeight').html(height);	// Display the height
};
$(document).ready(jqUpdateSize);	// When the page first loads
$(window).resize(jqUpdateSize);	 // When the browser changes size


function jqUpdateSizeBig(){
	// Get the dimensions of the viewport
	var width = $(window).width();
	var height = $(window).height();
	
	// Aggiunge la classe small-screen al body se height è inferiore a xxx
	if (height > 1000) {
		$(document.body).addClass('big-screen');
	} else {
		$(document.body).removeClass('big-screen');
	}
	
	//$('#jqWidth').html(width);	  // Display the width
	//$('#jqHeight').html(height);	// Display the height
};
$(document).ready(jqUpdateSizeBig);	// When the page first loads
$(window).resize(jqUpdateSizeBig);	 // When the browser changes size